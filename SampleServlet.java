package jiraplugin.servlet;
import java.util.*;
import java.lang.*;
import java.io.*;
import java.text.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.TimeUnit;


class SampleServlet extends HttpServlet
{
HashMap<String, HashMap<String, String>> trade = new HashMap<String, HashMap<String, String>>();
              
HashMap<String, HashMap<String, String>> trade_record = new HashMap<String, HashMap<String, String>>();
  
@Override
protected void doGet(HttpServletRequest req, HttpServletResponse response) 
                       throws ServletException, IOException {
    PrintWriter out = response.getWriter();
   Generate_trade();
   out.println("Sample Servlet code <p>");
    out.println( print_trade());
    out.println(claculate());
  
  }


/******************
*******************
GENERATE TRADE DATA TO PROCESS
********************
********************/
  
  public  void Generate_trade(){
      

trade.put("TEA", new HashMap<String, String>(){{

put("timestamp", new SimpleDateFormat("MM/dd/yyyy h:mm:ss:S a").format(new Date()));
put("quantity", "15");
put("buy_or-sell", "buy"); 
put("price", "100");
    
}});

trade_record.put("TEA", new HashMap<String, String>(){{
put("Type", "Common");
put("Last_Dividend", "0");
put("Fixed_Dividend", "0"); 
put("Par_Value", "100");
}});





trade.put("POP", new HashMap<String, String>(){{
put("timestamp", new SimpleDateFormat("MM/dd/yyyy h:mm:ss:S a").format(new Date()));
put("quantity", "15");
put("buy_or-sell", "buy"); 
put("price", "100");
}});
trade_record.put("POP", new HashMap<String, String>(){{
put("Type", "Common");
put("Last_Dividend", "8");
put("Fixed_Dividend", "0"); 
put("Par_Value", "100");
}});







trade.put("ALE", new HashMap<String, String>(){{
put("timestamp", new SimpleDateFormat("MM/dd/yyyy h:mm:ss:S a").format(new Date()));
put("quantity", "15");
put("buy_or-sell", "buy"); 
put("price", "100");
}});

trade_record.put("ALE", new HashMap<String, String>(){{
put("Type", "Common");
put("Last_Dividend", "23");
put("Fixed_Dividend", "0"); 
put("Par_Value", "60");
}});






trade.put("GIN", new HashMap<String, String>(){{
put("timestamp", new SimpleDateFormat("MM/dd/yyyy h:mm:ss:S a").format(new Date()));
put("quantity", "15");
put("buy_or-sell", "buy"); 
put("price", "100");
}});
trade_record.put("GIN", new HashMap<String, String>(){{
put("Type", "Preferred");
put("Last_Dividend", "8");
put("Fixed_Dividend", "2"); 
put("Par_Value", "100");
}});







trade.put("JOE", new HashMap<String, String>(){{
put("timestamp", new SimpleDateFormat("MM/dd/yyyy h:mm:ss:S a").format(new Date()));
put("quantity", "15");
put("buy_or-sell", "buy"); 
put("price", "100");
}});
trade_record.put("JOE", new HashMap<String, String>(){{
put("Type", "Common");
put("Last_Dividend", "13");
put("Fixed_Dividend", "0"); 
put("Par_Value", "250");
}});

 
      
  }

    
/******************
********************
Print Generated Data  
********************
********************/
  public String print_trade(){

      String var=("<style>td, th {width: 110px;}</style><p><table><tr><th>Stock</th><th>Type</th><th>L_Div</th><th>F_Div</th><th>Value</th></tr>");
      for (HashMap.Entry<String, HashMap<String, String>> entry : trade_record.entrySet())
{
  
    var+=("<tr><td>"+entry.getKey()
    +"</td><td> "+entry.getValue().get("Type")
    +"</td><td> "+entry.getValue().get("Last_Dividend")
    +"</td><td> "+entry.getValue().get("Fixed_Dividend")
    +"</td><td> "+entry.getValue().get("Par_Value")+"</td></tr>"
    );
}

        var+=("</table><p><table><tr><th> Stock</th><th> Buy or Sell</th><th> Quantity</th><th> Price</th><th> Date/Time</th></tr>");
      for (HashMap.Entry<String, HashMap<String, String>> entry : trade.entrySet())
{
  
    var+= ("<tr><td>"+entry.getKey()
    +"</td><td> "+entry.getValue().get("buy_or-sell")
    +"</td><td> "+entry.getValue().get("quantity")
    +"</td><td> "+entry.getValue().get("price")
    +"</td><td> "+entry.getValue().get("timestamp")+"</td></tr>"
    );
}

var+="<table>";


return var;
  }
  
  
/******************
********************
Calculate  
********************
********************/
    public String claculate(){
      String var="";
        float product_POP = 1.0f,productodTandQ_POP=0.0f,Q_POP=0.0f; int count_POP=0;
        float product_TEA = 1.0f,productodTandQ_TEA=0.0f,Q_TEA=0.0f;int count_TEA=0;
        float product_JOE = 1.0f,productodTandQ_JOE=0.0f,Q_JOE=0.0f;int count_JOE=0;
        float product_ALE = 1.0f,productodTandQ_ALE=0.0f,Q_ALE=0.0f;int count_ALE=0;
        float product_GIN = 1.0f,productodTandQ_GIN=0.0f,Q_GIN=0.0f;int count_GIN=0;

             var+=("</table><p><table><tr><th> Stock</th><th> Buy or Sell</th><th> Quantity</th><th> Price</th><th> Date/Time</th><th> Dividend </th><th> P/E Ratio</th></tr>");




      for (HashMap.Entry<String, HashMap<String, String>> entry : trade.entrySet())
{
  
   long minutes=0;
  try{
    var+= ("<tr><td>"+entry.getKey()
    +"</td><td> "+entry.getValue().get("buy_or-sell")
    +"</td><td> "+entry.getValue().get("quantity")
    +"</td><td> "+entry.getValue().get("price")
    +"</td><td> "+entry.getValue().get("timestamp")+"</td>"
    );

   minutes = TimeUnit.MILLISECONDS.toMinutes(new Date().getTime() - new SimpleDateFormat("MM/dd/yyyy h:mm:ss:S a").parse(entry.getValue().get("timestamp")).getTime());
  /************************************************
Check if Stock data was garther in last 15 minutes
  *************************************************/
  if(minutes<=15 && Float.parseFloat(entry.getValue().get("price"))!=0){
      float Dividend=1;
      if(trade_record.get(entry.getKey()).get("Type")=="Common"){
     Dividend= Float.parseFloat(trade_record.get(entry.getKey()).get("Last_Dividend"))/Float.parseFloat(entry.getValue().get("price"));   
      var+=("<td>  Dividend Yield for "+entry.getKey()+" is ("+trade_record.get(entry.getKey()).get("Last_Dividend")+"/"+entry.getValue().get("price")+") = "+Dividend +"</td>");
      }
       if(trade_record.get(entry.getKey()).get("Type")=="Preferred"){
      Dividend= ( (Float.parseFloat(trade_record.get(entry.getKey()).get("Fixed_Dividend")) /100) * Float.parseFloat(trade_record.get(entry.getKey()).get("Par_Value")) ) / Float.parseFloat(entry.getValue().get("price"))   ;
      var+=("<td>  Dividend Yield for "+entry.getKey()+" is (("+trade_record.get(entry.getKey()).get("Fixed_Dividend")+"/100)*"+trade_record.get(entry.getKey()).get("Par_Value")+"/"+entry.getValue().get("price")+") = "+Dividend+"</td>"  );
        
       }
     
    if(Dividend!=0) var+=("<td> P/E Ratio for "+entry.getKey()+" is ( "+entry.getValue().get("price")+"/"+Dividend+") = "+( Float.parseFloat(entry.getValue().get("price")) /Dividend) +"</td>");
    else
    var+=("<td> P/E Ratio for "+entry.getKey()+" is ( "+entry.getValue().get("price")+"/"+Dividend+") = 100%  </td>" );

  if(entry.getKey()=="TEA"){
      product_TEA = product_TEA *Float.parseFloat(entry.getValue().get("price"));
      productodTandQ_TEA=productodTandQ_TEA +(Float.parseFloat(entry.getValue().get("price")) *Float.parseFloat(entry.getValue().get("quantity"))   );
    Q_TEA=Q_TEA+Float.parseFloat(entry.getValue().get("quantity"));count_TEA++;
}

   if(entry.getKey()=="GIN"){

      product_GIN = product_GIN *Float.parseFloat(entry.getValue().get("price"));
      productodTandQ_GIN=productodTandQ_GIN +(Float.parseFloat(entry.getValue().get("price")) *Float.parseFloat(entry.getValue().get("quantity"))   );
    Q_GIN=Q_GIN+Float.parseFloat(entry.getValue().get("quantity"));    count_GIN++;
  }if(entry.getKey()=="ALE"){
      product_ALE = product_ALE *Float.parseFloat(entry.getValue().get("price"));
      productodTandQ_ALE=productodTandQ_ALE +(Float.parseFloat(entry.getValue().get("price")) *Float.parseFloat(entry.getValue().get("quantity"))   );
    Q_ALE=Q_ALE+Float.parseFloat(entry.getValue().get("quantity"));    count_ALE++;
 }if(entry.getKey()=="JOE"){
      product_JOE = product_JOE *Float.parseFloat(entry.getValue().get("price"));
      productodTandQ_JOE=productodTandQ_JOE +(Float.parseFloat(entry.getValue().get("price")) *Float.parseFloat(entry.getValue().get("quantity"))   );
    Q_JOE=Q_JOE+Float.parseFloat(entry.getValue().get("quantity"));    count_JOE++;
 }if(entry.getKey()=="POP"){
      product_POP = product_POP *Float.parseFloat(entry.getValue().get("price"));
      productodTandQ_POP=productodTandQ_POP +(Float.parseFloat(entry.getValue().get("price")) *Float.parseFloat(entry.getValue().get("quantity"))   );
    Q_POP=Q_POP+Float.parseFloat(entry.getValue().get("quantity"));    count_POP++;
      }
      var+="</tr>";
  }
      
  }
 catch(Exception e){
     var+=(e.toString());
 }
  
}
 var+="</table>";



 if(count_TEA>0) var+=("<p>  Geometric Mean for TEA the valueSet is  "+Math.pow(product_TEA, 1.0 / count_TEA));
var+=("<p>  Stock Price for   TEA valueSet is  "+(productodTandQ_TEA/Q_TEA));

  if(count_POP>0) var+=("<p>  Geometric Mean for POP the valueSet is  "+Math.pow(product_POP, 1.0 / count_POP));
var+=("<p>  Stock Price for   POP valueSet is  "+(productodTandQ_POP/Q_POP));

   if(count_JOE>0)var+=("<p>  Geometric Mean for JOE the valueSet is  "+Math.pow(product_JOE, 1.0 / count_JOE));
var+=("<p>  Stock Price for  JOE JOE valueSet is  "+(productodTandQ_JOE/Q_JOE));

   if(count_ALE>0)var+=("<p>  Geometric Mean for ALE the valueSet is  "+Math.pow(product_ALE, 1.0 / count_ALE));
var+=("<p>  Stock Price for   ALE valueSet is  "+(productodTandQ_ALE/Q_ALE));

  if(count_GIN>0) var+=("<p>  Geometric Mean for GIN the valueSet is  "+Math.pow(product_GIN, 1.0 / count_GIN));
var+=("<p>  Stock Price for   GIN valueSet is  "+(productodTandQ_GIN/Q_GIN));


  return var;
  }
  
}
